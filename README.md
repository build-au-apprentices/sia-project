# Apprentice Project
## About
- Authors: Isaac and Soth!
- Description: TBD :)
## Running the app
- Run `npm install` and `npm start` to run the app locally.
- Run `npm lint` to run eslint and `npm lint:fix` to run the command with the `--fix` tag. Prettier is recommended.

